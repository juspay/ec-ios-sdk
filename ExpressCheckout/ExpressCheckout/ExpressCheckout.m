//
//  ExpressCheckout.m
//  ExpressCheckout
//
//  Created by Sachin Sharma on 28/04/16.
//  Copyright © 2016 Juspay Technologies Pvt Ltd. All rights reserved.
//

#define EnvironmentString(enum) [@[@"PRODUCTION",@"SANDBOX"] objectAtIndex:enum]
#define EnvironmentUrl(enum) [@[@"https://api.juspay.in/merchant/ipay",@"https://sandbox.juspay.in/merchant/ipay"] objectAtIndex:enum]

#import "ExpressCheckout.h"

static NSString *LOG_TAG ;

@interface ExpressCheckout ()

@property (nonatomic, strong) JuspaySafeBrowser *browser;
@property (nonatomic) Environment environment;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) UIView *parentView;
@property (nonatomic, strong) NSString *merchantId;
@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, strong) NSString *instrumentOpts;
@property (nonatomic, strong) NSMutableArray *endUrlRegexes;

// Custom Payment View
@property (nonatomic, strong) UIView *netbankingView;
@property (nonatomic, strong) UIView *cardPaymentView;
@property (nonatomic, strong) UIView *walletView;
@property (nonatomic, strong) UIButton *paymentButton;

@end

@implementation ExpressCheckout

#pragma mark - Initialization Methods

- (id)init{
    
    self = [super init];
    
    if (self) {
        [self initializeDefaults];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self initializeDefaults];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    
    if (self) {
        [self initializeDefaults];
    }
    
    return self;
}

#pragma mark - Deintialization methods
//De intialize all that needs to be done

- (void)dealloc{
    
    [self deInitCurrentParams];
}

- (void)deInitCurrentParams{
    LOG_TAG = nil;
    self.browser = nil;
    self.environment = PRODUCTION;
    self.orderId = nil;
    self.merchantId = nil;
    self.endUrlRegexes = nil;
    self.parentView = nil;
    self.url = nil;
    self.instrumentOpts = nil;
}

#pragma mark - Defaults initialization
//Intialize defaults here.
- (void)initializeDefaults{
    
    LOG_TAG = NSStringFromClass([self class]);
    
    self.endUrlRegexes = [[NSMutableArray alloc]init];
    
    self.browser = [[JuspaySafeBrowser alloc]init];
    [self addSubview:self.browser];
    self.instrumentOpts = @"card|nb|wallet";
}

//TODO: add this to documentations
- (void)environment:(Environment)environment merchantId:(NSString*)merchantId orderId:(NSString*)orderID instrumentOptions:(NSArray*)intrumentOptions endUrlRegexes:(NSArray*)endUrlRegexes{
    
    self.merchantId = merchantId;
    self.orderId = orderID;
    self.instrumentOpts = [self stringFromArray:intrumentOptions];
    [self.endUrlRegexes addObjectsFromArray:endUrlRegexes];
    self.environment = environment;
    
    if (environment == SANDBOX) {
        [self.endUrlRegexes addObject:@"https://sandbox.juspay.in/*"];
    }
}

- (void)environment:(Environment)environment merchantId:(NSString*)merchantId orderId:(NSString*)orderID endUrlRegexes:(NSArray*)endUrlRegexes{
    
    self.merchantId = merchantId;
    self.orderId = orderID;
    [self.endUrlRegexes addObjectsFromArray:endUrlRegexes];
    self.environment = environment;
    
    if (environment == SANDBOX) {
        [self.endUrlRegexes addObject:@"https://sandbox.juspay.in/*"];
    }
}

- (void)startPaymentInView:(UIView*)view callback:(JPBlock)block{
    
    self.parentView = view;
    
    BrowserParams *parms = [[BrowserParams alloc] init];
    parms.orderId = [NSString stringWithFormat:@"%@",self.orderId];
    parms.merchantId = [NSString stringWithFormat:@"%@",self.merchantId];
    parms.url = [self generateURL];
    parms.clientId = [NSString stringWithFormat:@"%@",self.merchantId];
    parms.transactionId = [NSString stringWithFormat:@"%@",self.orderId];
    parms.endUrlRegexes = self.endUrlRegexes;
    
    [self.browser startpaymentWithJuspayInView:view withParameters:parms callback:^(BOOL status, NSError *error, id info) {
        
        [self.browser removeFromSuperview];
        
        block(status,error,info);
    }];
    [view bringSubviewToFront:self.browser];
}

- (NSString*)generateURL{
    
    NSMutableString *queryString = [NSMutableString new];

    [queryString appendString:[NSString stringWithFormat:@"?order_id=%@&merchant_id=%@",[self URLEncodedString_ch:[NSString stringWithFormat:@"%@",self.orderId]],[self URLEncodedString_ch:[NSString stringWithFormat:@"%@",self.merchantId]]]];
    
    if ([self URLEncodedString_ch:self.instrumentOpts]) {
        [queryString appendString:[NSString stringWithFormat:@"&payment_options=%@",[self URLEncodedString_ch:self.instrumentOpts]]];
    }
    
    return [NSString stringWithFormat:@"%@%@&mobile=true",EnvironmentUrl(self.environment),queryString];
}

- (NSString *)URLEncodedString_ch:(NSString*)string {
    NSMutableString * output = [NSMutableString string];
    const unsigned char * source = (const unsigned char *)[string UTF8String];
    int sourceLen = (int)strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

- (Environment)environmentEnumFromString:(NSString*)input{
    NSDictionary<NSString*,NSNumber*> *environment = @{
                      @"SANDBOX": @(SANDBOX),
                      @"PRODUCTION": @(PRODUCTION),
                      };
    return environment[input].intValue;
}

- (NSString*)stringFromArray:(NSArray*)array{
    
    NSMutableString *finalString = [[NSMutableString alloc] init];
    
    for (NSString *stringInArray in array) {
        if ([array lastObject]==stringInArray) {
            [finalString appendString:[NSString stringWithFormat:@"%@",stringInArray]];
        }else{
            [finalString appendString:[NSString stringWithFormat:@"%@|",stringInArray]];
        }
    }
    
    return finalString;
}

- (void)presentPaymentView {
    
    // Segmented Control
    NSArray *itemArray = [NSArray arrayWithObjects: @"NetBanking", @"CardPayment", @"Wallet", nil];
    UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
    segmentedControl.frame = CGRectMake(10, 30, _parentViewOne.frame.size.width-20, 30);
    [segmentedControl addTarget:self action:@selector(segmentAction:) forControlEvents: UIControlEventValueChanged];
    segmentedControl.selectedSegmentIndex = 0;
    [_parentViewOne addSubview:segmentedControl];
    
    _netbankingView = [[UIView alloc] initWithFrame:CGRectMake(0,80,_parentViewOne.frame.size.width,400)];
    _cardPaymentView = [[UIView alloc] initWithFrame:CGRectMake(0,80,_parentViewOne.frame.size.width,400)];
    _walletView = [[UIView alloc] initWithFrame:CGRectMake(0,80,_parentViewOne.frame.size.width,400)];
    [_netbankingView setBackgroundColor:[UIColor redColor]];
    [_cardPaymentView setBackgroundColor:[UIColor greenColor]];
    [_walletView setBackgroundColor:[UIColor blueColor]];
    
    //Payment Button
    _paymentButton = [[UIButton alloc] initWithFrame: CGRectMake(10, 500, _parentViewOne.frame.size.width-20, 30)];
    [_paymentButton setTitle:@"Make Payment" forState:UIControlStateNormal];
    [_paymentButton setTintColor:[UIColor whiteColor]];
    [_paymentButton setBackgroundColor:[UIColor redColor]];
    [_paymentButton addTarget:self action:@selector(staypressed:) forControlEvents:UIControlEventTouchUpInside];
    [_parentViewOne addSubview:_paymentButton];
    
    [self netbankingContianer:_parentViewOne];
}

- (void)segmentAction:(UISegmentedControl *)segment
{
    switch (segment.selectedSegmentIndex) {
        case 0:
            [self netbankingContianer:_parentViewOne];
            break;
        case 1:
            [self cardPaymentContianer:_parentViewOne];
            break;
        case 2:
            [self walletContianer:_parentViewOne];
            break;
        default:
            break;
    }
}

- (void) netbankingContianer: (UIView*)view {
    
    [view addSubview:_netbankingView];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setTitle:@"netbankingContianer" forState:UIControlStateNormal];
    button.frame = CGRectMake(10,10,100,100);
    [_netbankingView addSubview:button];
    
    [_paymentButton setBackgroundColor:[UIColor redColor]];
}

- (void) cardPaymentContianer: (UIView*)view {
    
    [view addSubview:_cardPaymentView];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setTitle:@"cardPaymentContianer" forState:UIControlStateNormal];
    button.frame = CGRectMake(10,10,100,100);
    [_cardPaymentView addSubview:button];
    
    [_paymentButton setBackgroundColor:[UIColor greenColor]];
}

- (void) walletContianer: (UIView*)view {
    
    [view addSubview:_walletView];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setTitle:@"walletContianer" forState:UIControlStateNormal];
    button.frame = CGRectMake(10,10,100,100);
    [_walletView addSubview:button];
    
    [_paymentButton setBackgroundColor:[UIColor blueColor]];
}

- (IBAction)staypressed:(id)sender {
    [self startPaymentInView:_parentViewOne callback:^(BOOL status, NSError *error, id info) {
        NSLog(@"%@",info);
    }];
}

@end
