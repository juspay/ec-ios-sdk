//
//  ViewController.swift
//  ExpressCheckoutSwiftDemo
//
//  Created by Sachin Sharma on 06/06/16.
//  Copyright © 2016 Juspay Technologies Pvt Ltd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var expressCheckout = ExpressCheckout();

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.expressCheckout.environment(PRODUCTION, merchantId: "YOUR_MERCHANT_ID", orderId: "YOUR_ORDER_ID", endUrlRegexes: ["YOUR_END_URLS"]);
        
        self.expressCheckout.startPaymentInView(self.view,callback: {(let result ,let error,let info) in
            print("INFO:",info);
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

