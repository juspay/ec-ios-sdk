//
//  ViewController.m
//  ExpressCheckout-Demo
//
//  Created by Sachin Sharma on 29/04/16.
//  Copyright © 2016 Juspay Technologies Pvt Ltd. All rights reserved.
//

#import "ViewController.h"
#import <ExpressCheckout/ExpressCheckout.h>

@interface ViewController ()

@property (nonatomic, strong) ExpressCheckout *checkout;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    self.checkout = [[ExpressCheckout alloc] init];
    [self.checkout environment:PRODUCTION merchantId:@"sachin_sharma" orderId:@"1463928175" endUrlRegexes:@[@"https:\\/\\/www.test.com\\/.*"]];
//    [self.checkout environment:SANDBOX_MOBILE_WEB merchantId:@"sachin_sharma" orderId:@"1461919377" endURLRegexes:@[]];
    
}

- (void)viewDidAppear:(BOOL)animated{
//    [self.checkout startPaymentInView:self.view callback:^(BOOL status, NSError *error, id info) {
//        NSLog(@"%@",info);
//    }];
    self.checkout.parentViewOne = self.view;
    [self.checkout presentPaymentView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
