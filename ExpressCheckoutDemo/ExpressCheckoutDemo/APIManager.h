//
//  APIManager.h
//  ExpressCheckoutDemo
//
//  Created by Sachin Sharma on 11/05/16.
//  Copyright © 2016 Juspay Technologies Pvt Ltd. All rights reserved.
//

#define NW [APIManager sharedManager]

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "Constants.h"

typedef void (^NetworkResponse)(NSURLSessionTask *operation, id json);
typedef void (^NetworkFailure)(NSURLSessionTask *operation, NSError *error);

@interface APIManager : NSObject
+(APIManager*) sharedManager;

- (void)accessRequestWithEndPoint:(NSString*)endpoint
                      requestType:(RequestType)requestType
                     requiresAuth:(Boolean)isAuthRequired
                       withParams:(NSDictionary*)params
                          success:(NetworkResponse)success
                          failure:(NetworkFailure)failure;

- (void)getApiAccessRequestURL:(NSString *)url withParams:(NSDictionary*)params
                       success:(NetworkResponse)success
                       failure:(NetworkFailure)failure;

@end
