//
//  DetailViewController.h
//  ExpressCheckoutDemo
//
//  Created by Sachin Sharma on 11/05/16.
//  Copyright © 2016 Juspay Technologies Pvt Ltd. All rights reserved.
//

typedef enum {
    CARD_ADD,
    CARD_LIST,
    CARD_DELETE,
    CARD_FINGERPRINT,
    ORDER_CREATE,
    ORDER_STATUS,
    ORDER_LIST,
    ORDER_UPDATE,
    ORDER_REFUND,
    TRANSACTION_CREDIT_DEBIT,
    TRANSACTION_NETBANKING
}ApiType;

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (nonatomic) ApiType api;
@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

