//
//  Constants.h
//  ExpressCheckoutDemo
//
//  Created by Sachin Sharma on 11/05/16.
//  Copyright © 2016 Juspay Technologies Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

#pragma mark - Custom Log
#ifdef DEBUG
#define DLog(...) NSLog(@"%s(%p) %@", __PRETTY_FUNCTION__, self, [NSString stringWithFormat:__VA_ARGS__])
#endif

typedef enum {
   kGET,
   kPOST
}RequestType;

#pragma mark - Numeric Constants
extern float kCornerRadius;

#pragma mark - Request Identifiers

#pragma mark - URLS
extern NSString * const kBaseURL;

#pragma mark - WebService EndPoints
extern NSString * const kCARD_ADD ;
extern NSString * const kCARD_LIST ;
extern NSString * const kCARD_DELETE ;
extern NSString * const kCARD_FINGERPRINT ;
extern NSString * const kORDER_CREATE ;
extern NSString * const kORDER_STATUS ;
extern NSString * const kORDER_LIST ;
extern NSString * const kORDER_REFUND ;
extern NSString * const kTRANSACTION ;

#pragma mark - Webservice Params
extern NSString * const kPARAM_MERCHANT_ID ;
extern NSString * const kPARAM_CUSTOMER_ID ;
extern NSString * const kPARAM_CUSTOMER_EMAIL ;
extern NSString * const kPARAM_CARD_NUMBER ;
extern NSString * const kPARAM_CARD_EXPIRY_YEAR ;
extern NSString * const kPARAM_CARD_EXPIRY_MONTH ;
extern NSString * const kPARAM_NAME_ON_CARD ;
extern NSString * const kPARAM_NICKNAME ;
extern NSString * const kPARAM_CARD_TOKEN ;
extern NSString * const kPARAM_CARD_REFRENCE ;
extern NSString * const kPARAM_CARD_FINGERPRINT ;

@end
