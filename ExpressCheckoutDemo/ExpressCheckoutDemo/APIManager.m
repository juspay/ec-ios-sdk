//
//  APIManager.m
//  ExpressCheckoutDemo
//
//  Created by Sachin Sharma on 11/05/16.
//  Copyright © 2016 Juspay Technologies Pvt Ltd. All rights reserved.
//

#import "APIManager.h"
#import "AppDelegate.h"

@interface APIManager ()

@property (nonatomic, strong) NSMutableArray *operations;
@property (nonatomic, strong) NSString *authKey;

@end

@implementation APIManager
+(APIManager*) sharedManager {
    static dispatch_once_t once;
    static APIManager * sharedInstance;
    dispatch_once(&once, ^{
        
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    
    if (self) {
        
        _operations = [[NSMutableArray alloc]init];
    }
    
    return self;
}

- (void)accessRequestWithEndPoint:(NSString*)endpoint
                            requestType:(RequestType)requestType
                            requiresAuth:(Boolean)isAuthRequired
                            withParams:(NSDictionary*)params
                               success:(NetworkResponse)success
                               failure:(NetworkFailure)failure{
    
    NSString *endURL = [NSString stringWithFormat:@"%@/%@",kBaseURL,endpoint];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    
    

    [manager POST:[NSURL URLWithString:endURL].absoluteString parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        success(task,responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        failure(operation,error);
    }];
}

- (void)getApiAccessRequestURL:(NSString *)url withParams:(NSDictionary*)params
                       success:(NetworkResponse)success
                       failure:(NetworkFailure)failure{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:[NSURL URLWithString:url].absoluteString parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

@end
