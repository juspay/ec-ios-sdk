//
//  DetailViewController.m
//  ExpressCheckoutDemo
//
//  Created by Sachin Sharma on 11/05/16.
//  Copyright © 2016 Juspay Technologies Pvt Ltd. All rights reserved.
//

#import "DetailViewController.h"
#import "Constants.h"
#import "APIManager.h"

NSString const *merchantID = @"sachin_sharma";

@interface DetailViewController ()

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
            
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        self.detailDescriptionLabel.text = [self.detailItem description];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString *endPoint;
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    switch (self.api) {
        case CARD_ADD:
        {
            endPoint = kCARD_ADD;
            [parameters addEntriesFromDictionary:
             @{
               kPARAM_MERCHANT_ID:merchantID,
               kPARAM_CUSTOMER_ID:@"juspay_test_customer",
               kPARAM_CUSTOMER_EMAIL:@"sachin.sharma@juspay.in",
               kPARAM_CARD_NUMBER:@"4242424242424242",
               kPARAM_CARD_EXPIRY_YEAR:@"2019",
               kPARAM_CARD_EXPIRY_MONTH:@"05",
               kPARAM_NICKNAME:@"Test",
               kPARAM_NAME_ON_CARD:@"Tester"
               }];
        }
            break;
        case CARD_LIST:
        {
            endPoint = kCARD_LIST;
            [parameters addEntriesFromDictionary:
             @{
               kPARAM_CUSTOMER_ID:@"juspay_test_customer",
               }];
        }
            break;
            
        default:
            break;
    }
    
    __weak DetailViewController *weakSelf = self;
    
    [NW getApiAccessRequestWithEndPoint:endPoint withParams:parameters success:^(NSURLSessionTask *operation, id json) {
        DLog(@"Response--%@",json);
        id description;
        switch (weakSelf.api) {
            case CARD_ADD:{
                   description = [NSString stringWithFormat:@"Card saved with %@",[json objectForKey:kPARAM_CARD_TOKEN]];
                }
                break;
                
            default:
                break;
        }
        self.detailItem = description;
        [self configureView];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        DLog(@"Response Error--%@",[error localizedDescription]);
        id description = [error localizedDescription];
        self.detailItem = description;
        [self configureView];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
