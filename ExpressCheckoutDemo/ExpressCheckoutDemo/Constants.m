//
//  Constants.m
//  ExpressCheckoutDemo
//
//  Created by Sachin Sharma on 11/05/16.
//  Copyright © 2016 Juspay Technologies Pvt Ltd. All rights reserved.
//

#import "Constants.h"

@implementation Constants

NSString * const kBaseURL = @"https://api.juspay.in";

#pragma mark - Numeric Constants

#pragma mark - Request Identifiers

#pragma mark - WebService EndPoints
NSString * const kCARD_ADD = @"card/add";
NSString * const kCARD_LIST = @"card/list";
NSString * const kCARD_DELETE = @"card/delete";
NSString * const kCARD_FINGERPRINT = @"card/fingerprint";
NSString * const kORDER_CREATE = @"order/create";
NSString * const kORDER_STATUS = @"order/status";
NSString * const kORDER_LIST = @"order/list";
NSString * const kORDER_REFUND = @"order/refund";
NSString * const kTRANSACTION = @"txns";

#pragma mark - Webservice Params
NSString * const kPARAM_MERCHANT_ID = @"merchant_id";
NSString * const kPARAM_CUSTOMER_ID = @"customer_id";
NSString * const kPARAM_CUSTOMER_EMAIL = @"customer_email";
NSString * const kPARAM_CARD_NUMBER = @"card_number";
NSString * const kPARAM_CARD_EXPIRY_YEAR = @"card_exp_year";
NSString * const kPARAM_CARD_EXPIRY_MONTH = @"card_exp_month";
NSString * const kPARAM_NAME_ON_CARD = @"name_on_card";
NSString * const kPARAM_NICKNAME = @"nickname";
NSString * const kPARAM_CARD_TOKEN = @"card_token";
NSString * const kPARAM_CARD_REFRENCE = @"card_reference";
NSString * const kPARAM_CARD_FINGERPRINT = @"card_fingerprint";

@end
