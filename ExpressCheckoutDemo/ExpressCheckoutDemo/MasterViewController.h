//
//  MasterViewController.h
//  ExpressCheckoutDemo
//
//  Created by Sachin Sharma on 11/05/16.
//  Copyright © 2016 Juspay Technologies Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;


@end

