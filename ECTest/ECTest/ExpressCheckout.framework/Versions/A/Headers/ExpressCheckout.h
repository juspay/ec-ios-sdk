//
//  ExpressCheckout.h
//  ExpressCheckout
//
//  Created by Sachin Sharma on 28/04/16.
//  Copyright © 2016 Juspay Technologies Pvt Ltd. All rights reserved.
//

typedef enum{
    DEFAULT,
    SANDBOX,
    PRODUCTION,
    SANDBOX_MOBILE_WEB,
    MOBILE_WEB
}Environment;

#import <UIKit/UIKit.h>
#import <JuspaySafeBrowser/JuspaySafeBrowser.h>

@interface ExpressCheckout : UIView

- (void)mobileWebCheckoutForEnvironment:(Environment)environment merchantID:(NSString*)merchantId orderID:(NSString*)orderID endURLRegex:(NSArray*)endUrlRegexes;
- (void)startPaymentInView:(UIView*)view callback:(JPBlock)block;

@end
